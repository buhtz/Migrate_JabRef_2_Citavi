#!/usr/bin/env python3
import sys
import pathlib
import cj_base
import pybtex
import pybtex.database


"""This script check the names of authors and editors in a given bib file
   and bring the first- and lastnames in the same order.
"""

__author__ = 'Christian Buhtz'
__date__ = 'March 2021'
__maintainer__ = __author__
__email__ = 'c.buhtz@posteo.jp'
__web__ = 'https://codeberg.org/buhtz/migrate_jabref_2_citavi'
__author_web__ = 'https://codeberg.org/buhtz/'
__license__ = 'GPLv3'
__version__ = '0.0.1a'
__app_name__ = 'Migrate JabRef 2 Citavi'

#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.


def parse_bib_file(filename):
    # read the raw content
    with open(filename, 'r', encoding='utf-8') as f:
        content = f.read()

    content = content.split('\n')

    result = {}

    # find beginning of JabRef meta-data
    for line in content:
        if line.startswith('@Comment{jabref-meta:'):
            idx = content.index(line)
            break

    jabref_meta = content[idx-1:]

    # remove empty lines
    try:
        while True:
            jabref_meta.remove('')
    except ValueError:
        pass

    # file-directory
    pattern = '@Comment{jabref-meta: fileDirectory:'
    for line in jabref_meta[:]:
        if line.startswith(pattern):
            # remove the line
            jabref_meta.remove(line)
            # extract the value
            line = line.replace(pattern, '')
            line = line.replace(';}', '')
            # store the value
            result['fileDirectory'] = line
            break

    # categories
    pattern = '@Comment{jabref-meta: grouping:'
    for line in jabref_meta:
        if line.startswith(pattern):
            idx = jabref_meta.index(line)
            break
    jabref_meta = jabref_meta[idx+1:-1]

    # extract and separate categorie fields
    categories = []
    for cat in jabref_meta:
        s = cat.split(';')
        idx = s[0].find(' ')
        level = s[0][0:idx]
        s.insert(0, level)
        s[1] = s[1][idx+1:]
        categories.append(s)

    # categories
    result['groups'] = []
    for cat in categories:
        level = int(cat[0])
        name = cat[1]
        # ignore this group
        if name == 'AllEntriesGroup:':
            continue
        # allow static groups only
        if not name.startswith('StaticGroup:'):
            raise Exception(f'{name} is not a StaticGroup.')
        # extract the name only
        name = name[12:-1]
        # misc
        misc = cat[2:]
        # assemble result
        result['groups'].append(
            {
                'level': level,
                'name': name,
                'misc': misc
            }
        )

    return result


def print_group_structure(bib_struct):
    """
    """
    for e in bib_struct['groups']:
        print('{}lvl:{} name:{} - misc:{}'
              .format('  '*(e['level']-1), e['level'], e['name'], e['misc']))


def sort_citavientries_into_categories(bib_entries):
    """Sort Citavi entries into their catetories.

    Args:
        bib_entries (dict): BibtexKey indexed dictionary of bib-file entries.
    """
    controller = cj_base.BasicController.me

    # iterate over all bib-entries
    for bibkey in bib_entries:
        # bib-file entry
        entry = bib_entries[bibkey]
        # and the entry's groups
        try:
            groups = entry.fields['groups'].split(', ')
        except KeyError:
            # report
            print('Attention! The entry {} does not belong to a group.'
                  .format(bibkey))
            groups = []

        # get the corrosponding citavi entry
        reference = controller.Get_Reference_ByBibTexKey(bibkey)

        # iterate over the groups of current bib-entry
        for g in groups:
            try:
                # find corrosponding Citavi category
                cat = controller.Get_Category(catids[g])
                # sort the citavi entry into that category
                reference._category.append(cat)
            except KeyError:
                # JabRef remembers related groups that are still deleted
                pass


def correct_attachments(bib_entries, jabref_dir, citavi_dir):
    """
    Args:
        bib_entries (dict): BibtexKey indexed dictionary of bib-file entries.
    """
    controller = cj_base.BasicController.me
    all_refs = controller.QueryAll(cj_base.Reference)

    for reference in all_refs:
        # get corrosponding bib-entry
        try:
            entry = bib_entries[reference._bibtexkey]
        except KeyError:
            # This happens when the Reference object was created automaticlly
            # as a Collection for a corrosponding @InCollection bib-entry
            continue
        except AttributeError:
            # This happens when the @InCollection bib-entry does not
            # contain enough informations (booktitle, editor, ...) so that
            # Citavi can create a valid Collection reference out of it.
            # The Reference is created but because of missing informations
            # without a bibtex-key.
            print('WARNING: There is a Reference without bibtexkey. '
                  'It is ignored {}'
                  .format(reference))
            continue

        # There is a bug(?) in Citavis import filter creating dead links
        # to local files.
        reference.RemoveUnexistingLocations()

        # is there an attachment?
        try:
            reference.Add_Attachments(jabref_dir,
                                      entry.fields['file'],
                                      citavi_dir)
        except KeyError:
            pass


def sort_collections_into_categories():
    """
    """
    controller = cj_base.BasicController.me

    # find all reference that are not sort into a category
    all_refs = controller.Get_Reference_WithoutCategory()

    # iterate the references
    for reference in all_refs:
        # each child of that reference
        for child in reference._children:
            # each category of that child
            for cat in child._category:
                # add reference to child's category
                reference._category.append(cat)


def create_citavi_categories(bib_struct):
    """Create the categories (groups) in a Citavi project file.
    """
    # dictionary to associate bib-group-names with citavi-keys
    # the group names in a JabRef bib file are always unique
    catids = {}

    # iterate over bib-file groups
    cur_level = 0
    last_cat = None
    parents = {0: None}
    for group in bib_struct['groups']:
        # level of group
        level = group['level']-1

        # level downstairs?
        if level > cur_level:
            cur_level = level
            # remember category from previous iteration as
            # parent of this new level
            parents[cur_level] = last_cat
        # level upstairs (back)?
        elif level < cur_level:
            cur_level = level

        # create category in Citavi
        cat = cj_base.Category(name=group['name'], parent=parents[level])

        # remember that category as possible parent
        last_cat = cat

        # associate the categories IDs with its names
        catids[group['name']] = cat._id

    return catids


if __name__ == '__main__':
    # status
    print('{} {} ({}) \n\tMaintained by {} ({}) {}\n\tAt {}\n\tLicence: {}'
          .format(__app_name__,
                  __version__,
                  __date__,
                  __author__,
                  __author_web__,
                  __email__,
                  __web__,
                  __license__))

    print('PREPARING...')

    # check arguments
    if len(sys.argv) != 3:
        print('Please give a name of bib-file and a citavi-file.')
        sys.exit(1)

    # bib and citavi file
    for idx in [1, 2]:
        if sys.argv[idx].endswith('.bib'):
            bib_filename = sys.argv[idx]
        elif sys.argv[idx].endswith('.ctv6'):
            ctv_filename = sys.argv[idx]

    # status
    print(f'Using bib-file {bib_filename} and citavi-file {ctv_filename}.')

    # Citavi attachment folder
    citavi_dir = pathlib.Path(ctv_filename).absolute()
    citavi_dir = citavi_dir.parent
    citavi_dir = citavi_dir.joinpath('Citavi Attachments')
    if not citavi_dir.exists():
        raise FileNotFoundError(citavi_dir)
    print('Using {} as attachment folder for Citavi.'.format(citavi_dir))

    # parse categories/groups in bib file
    bib_struct = parse_bib_file(bib_filename)

    # JabRef attachment folder
    jabref_dir = pathlib.Path(bib_struct['fileDirectory'])
    if not jabref_dir.is_absolute():
        jabref_dir = jabref_dir.resolve()
    if not jabref_dir.exists():
        raise FileNotFoundError(jabref_dir)
    print('Using {} as attachment folder for JabRef.'.format(jabref_dir))

    # status
    print('OPENING & READING Citavi and JabRef file...')

    # Open project file and establish database connection
    controller = cj_base.BasicController()
    controller.SetDatabase(ctv_filename)

    # read bib content
    bib_content = pybtex.database.parse_file(bib_filename, 'bibtex')

    # status
    print('CREATING CATEGORIES...')

    # re-create the group structure in Citavi project file
    catids = create_citavi_categories(bib_struct)
    controller.session.commit()

    # status
    print('SORTING Citavi Reference\'s INTO CATEGORIES...')

    # add the citavi-entries to the correct categories
    sort_citavientries_into_categories(bib_content.entries)

    # status
    print('HANDLING ATTACHMENTS...')

    # copy JabRef attachments and clean dead links
    correct_attachments(bib_content.entries, jabref_dir, citavi_dir)

    # status
    print('HANDLING Citavi\'s Collection Reference\'s WITHOUT CATEGORIES ...')

    # Citavi auto-create entries (Reference) for @InCollection bib-entries
    # and similar.
    # They have to go the the same citavi category then their children.
    sort_collections_into_categories()

    # status
    print('COMMIT & CLOSE...')

    # commit changes and close database
    controller.session.commit()
    controller.SessionClose()

    # status
    print('DONE. Bye Bye.')
