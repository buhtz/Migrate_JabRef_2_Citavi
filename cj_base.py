#!/usr/bin/env python3
"""
    A sub modul with basic functions realted to that project.
"""
import os
import logging
import uuid
import json
import pathlib
import shutil
import sqlalchemy as sa
import sqlalchemy.orm as sao
import sqlalchemy.sql as sas
import sqlalchemy.inspection as sai
import sqlalchemy.ext.declarative as sad
from sqlalchemy_utils.types.uuid import UUIDType


__author__ = 'Christian Buhtz'
__date__ = 'March 2021'
__maintainer__ = __author__
__email__ = 'c.buhtz@posteo.jp'
__web__ = 'https://codeberg.org/buhtz/migrate_jabref_2_citavi'
__author_web__ = 'https://codeberg.org/buhtz/'
__license__ = 'GPLv3'


#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

_Base = sad.declarative_base()


def _model_str(model):
    """
    """
    # this will raise an sqlalchemy.exc.NoInspectionAvailable
    # if it is not an sqlalchemy mapped object
    insp = sai.inspect(model)
    attrs = insp.attrs

    section_seperator = '\n\t'

    # class and id
    mstr = '{} at {}:'.format(type(model), id(model))
    mstr += section_seperator

    # states
    mstr += ('HAS ' if insp.has_identity else 'has NO ')
    mstr += 'identity  '
    for s in ['detached', 'persistent', 'pending', 'transient']:
        mstr += ('IS ' if getattr(insp, s) else 'NOT ') + s
        mstr += '  '
    # is_modified
    session = sao.Session.object_session(model)
    if session:
        mstr += ('IS ' if session.is_modified(model) else 'NOT ') + 'modified'
    mstr += section_seperator

    # attribute
    for attr in attrs:
        mstr = mstr + ' {}="{}"'.format(attr.key, attr.value)

    return mstr


ReferenceAuthor = sa.Table(
    'ReferenceAuthor', _Base.metadata,
    sa.Column('ReferenceID',  sa.Integer,
              sa.ForeignKey('Reference.ID'), primary_key=True),
    sa.Column('PersonID', sa.Integer,
              sa.ForeignKey('Person.ID'), primary_key=True),
    sa.Column('Index', sa.Integer)
)

ReferenceCategory = sa.Table(
    'ReferenceCategory', _Base.metadata,
    sa.Column('ReferenceID', UUIDType,
              sa.ForeignKey('Reference.ID'), primary_key=True),
    sa.Column('CategoryID', UUIDType,
              sa.ForeignKey('Category.ID'), primary_key=True)
)

ReferenceReference = sa.Table(
    'ReferenceReference', _Base.metadata,
    sa.Column('ParentReferenceID', UUIDType,
              sa.ForeignKey('Reference.ID'), primary_key=True),
    sa.Column('ChildReferenceID', UUIDType,
              sa.ForeignKey('Reference.ID'), primary_key=True)
)

CategoryCategory = sa.Table(
    'CategoryCategory', _Base.metadata,
    sa.Column('ParentCategoryID', UUIDType, sa.ForeignKey('Category.ID')),
    sa.Column('SubcategoryID', UUIDType, sa.ForeignKey('Category.ID')),
    sa.Column('Index', sa.Integer, default=0)
)


class Category(_Base):
    __tablename__ = 'Category'

    _id = sa.Column('ID', UUIDType, primary_key=True)
    _createdby = sa.Column('CreatedBy', sa.String)
    _createdon = sa.Column('CreatedOn', sa.DateTime(timezone=True),
                           default=sas.func.now())
    _name = sa.Column('Name', sa.String)
    _staticids = sa.Column('StaticIDs', sa.String, default='')

    def __init__(self, name, parent=None):
        super(Category, self).__init__()

        # ID
        self._id = uuid.uuid4()
        self._staticids = f'["{self._id}"]'
        # Name
        self._name = name
        # Create infos
        self._createdby = os.path.split(os.path.expanduser('~'))[-1]
        # parent
        self._set_tree_parent(parent)
        # add to database
        BasicController.me.session.add(self)

    def __repr__(self):
        return _model_str(self)

    def _get_next_free_index(self, category):
        """Find a free index (as integer) for childs in 'category'.
        """

        # query all childs in the category
        questions = _Base.metadata.tables['CategoryCategory']
        if category:
            filter_id = category._id
        else:
            filter_id = None
        stmt = BasicController.me \
                              .session.query(questions) \
                              .filter(questions.c.
                                      ParentCategoryID == filter_id)
        res = BasicController.me.session.execute(stmt)

        # iterate over them and find the highest index
        idx = -1

        for r in res:
            if r[2] > idx:
                idx = r[2]

        # return the next possible/free index
        return idx+1

    def _set_tree_parent(self, parent):
        # find free index in 'parent'
        index = self._get_next_free_index(parent)

        if parent:
            parent = parent._id

        # create insert statement
        stmt = sa.insert(CategoryCategory).values(ParentCategoryID=parent,
                                                  SubcategoryID=self._id,
                                                  Index=index)
        # execute and evaluate
        res = BasicController.me.session.execute(stmt)
        if res.rowcount != 1:
            raise Exception(f'Something wrong with {stmt}.')


class Reference(_Base):
    __tablename__ = 'Reference'

    _id = sa.Column('ID', UUIDType, primary_key=True)
    _bibtexkey = sa.Column('BibTeXKey', sa.String)
    _title = sa.Column('Title', sa.String)
    _year = sa.Column('Year', sa.String)
    _doi = sa.Column('DOI', sa.String)
    _pmid = sa.Column('PubMedID', sa.String)
    _url = sa.Column('OnlineAddress', sa.String)
    _note = sa.Column('Notes', sa.String)
    _evaluation = sa.Column('Evaluation', sa.String)  # Bewertung
    _blue_mark = sa.Column('HasLabel1', sa.Boolean)  # blauer Kreis
    _red_mark = sa.Column('HasLabel2', sa.Boolean)  # rotes Fähnchen

    # parent / Sammelwerk
    _parent = sao.relationship(
        'Reference',
        secondary=ReferenceReference,
        primaryjoin=_id == ReferenceReference.c.ChildReferenceID,
        secondaryjoin=_id == ReferenceReference.c.ParentReferenceID,
        backref='_children'
    )

    # Autor
    _authors = sao.relationship(
        'Person', secondary=ReferenceAuthor,
        order_by=ReferenceAuthor.c.Index
    )

    # Category
    _category = sao.relationship('Category', secondary=ReferenceCategory)

    # Journal
    _periodical_fk = sa.Column('PeriodicalID',
                               sa.Integer,
                               sa.ForeignKey('Periodical.ID'))
    _periodical = sao.relationship('Periodical')

    # Location (attachment)
    _location = sao.relationship('Location')

    def __init__(self):
        super(Reference, self).__init__()

    def __repr__(self):
        return _model_str(self)

    def GetShortStringRepr(self):
        """
        """
        return '({}, {}) {}...'.format(
                    self._authors[0]._lastname,
                    self._year,
                    self._title[0:20])

    def GetAuthorStringList(self, fullname=True):
        """
        """
        result = ''
        # each author
        for a in self._authors:
            result += '; ' + (a.fullname if fullname else a.lastname)
        if result[0:2] == '; ':
            result = result[2:]
        return result

    def Add_Attachments(self, jabref_dir, files_field, citavi_dir):
        """Files from the JabRef folder ('jabref_dir') are copied
        to the Citavi attachment folder ('citavi_dir').

        Args:
            jabref_dir (pathlib.Path): JabRef attachment folder.
            fiels_field (String): Raw content of 'file' bib-field.
            citavi_dir (pathlib.Path): Citavi attachment folder.

        JabRef store informations about files attached to an entry
        in the 'file' field. Multiple files are separated via ';'. Each
        file is described by three ':' spearted fields: description,
        filename and filetype.
        Example:
            file = {Paper:paper.pdf:PDF;Picture:pic.png:PNG}
        """
        # each file entry
        for f in files_field.split(';'):
            # extract the filename and notes
            fn = f.split(':')
            notes = fn[0]
            fn = fn[1]

            # copy file
            src = jabref_dir.joinpath(fn)
            dest = citavi_dir.joinpath(fn)
            try:
                shutil.copy(src, dest)

                # create location
                location = Location(fn, notes)
                self._location.append(location)
            except FileNotFoundError:
                print('Ignoring unexisting attachment for {}: {}'
                      .format(self._bibtexkey, src))

    def RemoveUnexistingLocations(self):
        """
        """
        to_remove = []
        for loc in self._location:
            if not loc.exists_or_clean():
                to_remove.append(loc)

        for loc in to_remove:
            self._location.remove(loc)
            BasicController.me.session.delete(loc)


class Location(_Base):
    __tablename__ = 'Location'

    _id = sa.Column('ID', UUIDType, primary_key=True)
    _address = sa.Column('Address', sa.String)
    _notes = sa.Column('Notes', sa.String)

    _reference_fk = sa.Column('ReferenceID', UUIDType,
                              sa.ForeignKey('Reference.ID'))

    def __init__(self, filename, notes):
        super(Location, self).__init__()

        # ID
        self._id = uuid.uuid4()

        # file
        address = {
            "$id": "1",
            "$type": "SwissAcademic.Citavi.LinkedResource, SwissAcademic.Citavi",
            "LinkedResourceType": 1,
            "UriString": filename,
            "LinkedResourceStatus": 8
        }
        self._address = json.dumps(address)

        # notes
        if notes:
            self._notes = notes

    def __repr__(self):
        return _model_str(self)

    def exists_or_clean(self):
        """Check if local files (type 3) are exists.

        This is not for files in Citavis Attachment (type 1) folder,
        WebLinks (type 5) or other location types.
        """
        address = json.loads(self._address)

        # Do not types other then local-files (type 3)
        if not address['LinkedResourceType'] == 3:
            return True

        # does the file exists
        fn = pathlib.Path(address['UriString'])

        try:
            result = fn.exists()
        except OSError:
            result = False

        return result


class Periodical(_Base):
    __tablename__ = 'Periodical'

    _id = sa.Column('ID', sa.Integer, primary_key=True)
    _issn = sa.Column('ISSN', sa.String)
    _name = sa.Column('Name', sa.String)

    def __repr__(self):
        return _model_str(self)


class Person(_Base):
    __tablename__ = 'Person'

    _id = sa.Column('ID', sa.Integer, primary_key=True)
    _firstname = sa.Column('FirstName', sa.String)
    _lastname = sa.Column('LastName', sa.String)
    # Sex: 0=Unbestimmt, 1=Weiblich, 2=Männlich, 3=Neutral
    _sex = sa.Column('Sex', sa.String)

    def __repr__(self):
        return _model_str(self)

    @property
    def fullname(self):
        return '{}, {}'.format(self._lastname, self._firstname)

    @property
    def lastname(self):
        return self._lastname


class BasicController():
    """Might be session handling.
    """

    me = None

    def __init__(self, model=None, sql=False):
        if BasicController.me:
            raise Exception('Only one instance of BasicController allowed!')
        else:
            BasicController.me = self

        self._session = None
        self._session_str = None
        self._model = model
        self._autoflush = True
        self._sql = sql

    def __del__(self):
        self.SessionClose()

    def SetDatabase(self, filename):
        """
        """
        # does the file exist?
        if not os.path.exists(filename):
            raise FileNotFoundError(filename)

        # close old session if one is open
        if self._session is not None:
            self._session.close()

        # new session string
        self._session_str = 'sqlite:///{}'.format(filename)

    def SetModel(self, model):
        """
        """
        if self.model is not None:
            raise AttributeError(
                '_model can not be set because there is still one set.'
            )

        self._model = model
        self.session.add(self._model)
        return self._model

    def CreateSession(self):
        """
            Create a new Session object.
            If it is still their an AtrributeError is raised.
            Overload this methode to create a different Session.
        """
        if self._session is not None:
            raise AttributeError(
                '_session can not be created because it is still set.'
            )

        engine = self.GetDBEngine()
        self._session = sao.sessionmaker(bind=engine)()
        logging.debug('Created a DB-Session with session '
                      'string: {}'.format(self._session_str))

    def GetDBEngine(self):
        """
            Helper methode. Engine object should be given and managed by a
            application object.
        """
        return sa.create_engine(self._session_str, echo=self._sql)

    def AutoFlushTemporaryOff(self):
        """
        """
        self._autoflush = False

    @property
    def autoflush(self):
        """Return the current autoflush value and reset
        it to its default ('True') after reading.

        Please see AutoFlushTemporaryOff() for details."""
        af = self._autoflush
        self._autoflush = True
        return af

    @property
    def session(self):
        """
            Getter for BasicController._session.
        """
        if not self._session:
            self.CreateSession()

        return self._session

    @property
    def model(self):
        """
            Getter for BasicController._model.
        """
        return self._model

    def SessionCommit(self):
        """"""
        self.session.commit()

    def SessionClose(self):
        """
        """
        if self._session:
            self._session.close()
        self._session = None

    def SessionRollback(self):
        """"""
        self.session.rollback()

    def IsModelModified(self, model=None):
        """
        Return True if the model is modified compared against its entity
        in the database.

        Per default the controllers own 'self._model' would be used.
        Otherwise use the parameter 'model'.

        :param model: Model

        :return True|False: Return
        """
        # use the controller-specific 'self._model'
        # or the parameter 'model' if it is givin
        m = model if model else self._model

        # is it a list of models?
        if isinstance(m, list):
            for i in m:
                if self.session.is_modified(m):
                    return True
            return False
        else:
            return self.session.is_modified(m)

    def AddObject(self, obj):
        """
        Add 'obj' to the Session. It is not commited.
        """
        self.session.add(obj)

    def QueryAll(self, tableclass=None, orderby=None):
        """
            Return all rows of the specified table-class or a
            given model type as a list of SQLAlchemy objects.
        """
        if tableclass is None:
            tableclass = type(self._model)

        # the class/table
        query = self.session.query(tableclass)
        # order?
        if orderby:
            query = query.order_by(orderby)
        # autoflush
        query = query.autoflush(self.autoflush)
        # do it!
        return query.all()

    def CountRows(self, tableclass):
        """return the row count of the specified table-class"""
        query = self.session.query(tableclass)
        query = query.autoflush(self.autoflush)
        return query.count()

    def Get_Category(self, cid):
        """
        """
        query = self.session.query(Category).filter_by(_id=cid)
        return query.one()

    def Get_Reference_ByBibTexKey(self, bibtexkey):
        """
        """
        query = self.session.query(Reference) \
                    .filter_by(_bibtexkey=bibtexkey)
        return query.one()

    def Get_Reference_WithoutCategory(self):
        """
        """
        query = self.session.query(Reference) \
                    .filter_by(_category=None)
        return query.all()
