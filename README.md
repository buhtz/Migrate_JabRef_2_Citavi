# Migrate JabRef to Citavi

A simple python script helps you migrating your bibliographic data (including attachments and groups/categories) from [JabRef](https://www.jabref.org/) into [Citavi](https://www.citavi.com).

# Steps to migrate

## Prepare JabRef's bib-file
 1. Get the bib file. You can use example.bib for a first warming up test.
 1. Convert all none-StaticGroups (e.g. keyword groups) to StaticGroups via
    _Edit Group_ -> _Collect by: Explicit collection_ -> And click _OK_ when
    asked _Assign the original group's entries to this group?_
    You can check if all groups are StaticGroup when open the bib file in all
    text editor and look at the end of the file.

 ![JabRef StaticGroups](doc/01_jabref_staticgroups.gif "JabRef StaticGroups")

## Import the bib file in Citavi via Citavi's own import feature.
 1. Create an empty Citavi project.
 1. _Extras_ -> _Options_ -> _Citations_ -> activate _LaTeX-Support_
 1. _Title_ -> _Import_ -> _Import from text file_
 1. Add new text-filter and use it -> "BibTeX (unknown fields to Notes)"
 1. In the next dialog window take care of encoding (maybe "UTF-8"?) and
    the correct order for first- and lastname. If you have both forms
    ("John Doe" and "Doe, John") in your bib-file I recommand to
    use "Doe, John" while Citavi import. Citavi is able to recognize
    and transform not all but most of the "John Doe" forms, too.
 1. Uncheck _overwrite bibtex keys_. The keys in Citavi need to be the
    same as in the bib-file.
 1. Close Citavi and the Citavi project.

 ![Citavi Import](doc/02_citavi_import.gif "Citavi Import")


## Prepare your Python3 environment
 - Install [Python3](https://python.org) if needed.
 - Install this additational python packages via package manager of your choice.
    - [sqlalchemy](https://docs.sqlalchemy.org)
    - [sqlalchemy_utils](https://sqlalchemy-utils.readthedocs.io)
    - [pybtex](https://docs.pybtex.org/)

## Run the script
Run the script `migrate_jabref_2_citavi.py` (wihtout the `.py`) and give the name's of the
JabRef and Citavi file as a parameter to it.

Example:
   
    py -3 -m migrate_jabref_2_citavi example.bib myliterature.ctv6

Possible problems can happen with JabRefs file directory. The script is build that way that it recoginize this problems and inform you about it. But please [open a bug report](https://codeberg.org/buhtz/Migrate_JabRef_2_Citavi/issues) if your use case is not handled.

Prepare yourself to run the script multiple times because you will ran into
parsing problems. JabRef allow to create bad and damaged bib-files. But the
pybtex package that is used by the script for parsing that bib-file is much
more accurate and will throw Exceptions when something is wrong. For example
missing or duplicated bibtex-keys, wrong characters (e.g. HTML-stuff), to
many commas in journal titles, etc.

# Contact

Please feel free to contact me via the Codebergs [issue system](https://codeberg.org/buhtz/Migrate_JabRef_2_Citavi/issues) or direct.
Find more contact details in the py-source file.
